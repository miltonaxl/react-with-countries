import React, { useEffect, useState } from 'react';
import './index.css'
import NavbarComponent from '../nav/index';
import { Link } from 'react-router-dom'
import {e} from '../../config/enviroments';
import ErrorComponent from '../error';

interface InFlags { }

interface NameCuntry {
  name: string,
  capital: string,
  region: string,
  callingCodes: [],
  languages: [],
  flag: string,
  currencies: [], 
}
const Flags: React.FC<InFlags> = () => {
  let titleValue: string = 'Countries'; 
  let title = document.querySelector('title') as HTMLElement;

  const [getCountries, setCountries] = useState([]);
  const [getFirtsState, setFirtsState] = useState<[]>([]);
  const [getSearching, setSearching] = useState([]);
  //states filter and searching options
  const [getRegion, setRegion] = useState(String);
  const [getSearch, setSearch] = useState(String);

  const filterByRegion = async() => {

    if (getRegion !== '') {
      const getNewCountries = getFirtsState.filter(({ region }) => region === getRegion);
      setCountries(getNewCountries);
      setSearching(getNewCountries);
    } else if (getFirtsState.length === 0) {
      const api = await fetch(`${e.API_URL}/all`);
      const countries = await api.json();
      setCountries(countries)
      setFirtsState(countries);
    } else {     
      allCountries();
    }
  }
  
  useEffect(() => {
    filterByRegion();  
  }, [getRegion])

  useEffect(() => {
    filter();
  },[getSearch])
  
  const filter = () => {
    if (getSearch !== '') {
      
      const  countrySearch:[] = [];
      getFirtsState.map(country => {
        const {name}: NameCuntry = country;
        if (name.toLocaleLowerCase().indexOf(getSearch.toLowerCase()) !== -1) {
          countrySearch.push(country);
        } 
      });
      setCountries(countrySearch);
    } else if (getRegion !== '') {
      titleValue = getRegion;
      setCountries(getSearching)
    } else {
      allCountries();
    }
  }
  const allCountries = () => {
    setCountries(getFirtsState);
  }   
  title.innerHTML = `${titleValue}`;
  return (
    <>
      <NavbarComponent
        setSearch={setSearch}
        setRegion={setRegion}
      />
    <div  className="container">
      {
        getCountries.length !== 0? getCountries.map((country) => {
          const { name, flag, capital, region, languages, currencies, callingCodes }:NameCuntry = country
          return (
              <div  key={name} className="card">
                <div className="card-head">
                      <img loading="lazy" className="img-flag" src={flag} alt="Colombia"/>
              </div>
              <div className="card-body">
              
             
                  <h1 className="title">{name}</h1>
                  <ul>
                    <li> 
                      Capital : <span>{capital}</span>
                    </li>
                    <li >
                      Region : <span className="region-name"> {region} </span>
                    </li>
                    <li>
                    Language :
                    {
                      <span> {languages.map(({ name }) =>  `${name} `)} </span>
                    }
                    </li>
                    <li>
                        Currencie : {
                      <span> { currencies.map(({code})=> code )} </span>
                    
                    }
                      </li>
                      <li>
                      Calling code : {
                      <span> { callingCodes.map( c => c) } </span>
                    
                    }
                  </li>
                  <Link to={`/countries/${name}`}> <li className="view-country">Click view country</li> </Link>
                    
                  </ul>
                </div>
              </div>
          );
        }) :
        <ErrorComponent/>
      }
      </div>
      </>
  );
}
export default Flags;