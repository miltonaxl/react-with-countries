import React from 'react';
import './index.css'

const options = [
  {
    value: '',
    name:'Select by region'
  },
  {
    value: 'Americas',
    name: 'Americas'
  },
  {
    value: 'Asia',
    name: 'Asia'
  },
  {
    value: 'Africa',
    name: 'Africa'
  },
  {
    value: 'Polar',
    name: 'Polar'
  },
  {
    value: 'Oceania',
    name: 'Oceania'
  },
  {
    value: 'Europe',
    name: 'Europe'
  }
]


interface InNavbarComponent {
  setRegion: React.Dispatch<React.SetStateAction<string>>,
  setSearch: React.Dispatch<React.SetStateAction<string>>
}

const NavbarComponent: React.FC<InNavbarComponent> = (props) => {
  const { setRegion, setSearch } = props;
  
  const selectCountry = (e:any) => {
    setRegion(e.target.value);
  }

  const inputSearch = (e: any) => {
    const capitalizerSearch: string = e.target.value;
    setSearch(capitalizerSearch);
  }

  return (
    <div className="navbar">
      <select onChange={ selectCountry}>
        {
          options.map(({ name, value }) => {
            return (
              <option key={value} value={value}>{name}</option>
            );
          })
        }
      </select>
      <div  className="form-group">
        <input  onKeyUp={ inputSearch} type="text" placeholder="Search by name" className="form-control" />
      </div>
    </div>
  );
}

export default NavbarComponent;