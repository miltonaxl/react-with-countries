import React from 'react';
import { BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Flag from './Flag/index';
import Flags from './Flags/index';


interface InRoutes {}
const Routes: React.FC<InRoutes> = () => {

  const local = window.location.pathname;
  if (local === '/') window.location.href = '/countries';

  return (

    <Router>
      <Switch>
        <Route exact path="/countries">
          <Flags />
        </Route>
        <Route path="/countries/:id">
          <Flag/>
        </Route>
      </Switch>
    </Router>

    
  ); 
}


export default Routes;