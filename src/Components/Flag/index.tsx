import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import './index.css';
import {e} from '../../config/enviroments';
import ErrorComponent from '../error';
import Map from '../map/index';
interface ParamType{
  id: string
}

interface InFlag {
  
 }
interface InCountry{
    name: string,
    capital: string,
    region: string,
    callingCodes: [],
    languages: [],
    flag: string,
    currencies: [],
    borders: [],
    latlng:Array<[]>
}
const Flag:React.FC<InFlag> =  ()=> {


  
  

  let { id } = useParams<ParamType>();

  let title = document.querySelector('title') as HTMLElement;
  const fLetter = id.charAt(0).toUpperCase();
  const endLetter = id.slice(1);
  title.innerHTML = `${fLetter}${endLetter}`;
  let link = document.querySelector('link') as HTMLElement;
  link.removeAttribute('href');
  

  const getCountryInfo = async () => {
    try {
      const api = await fetch(`${e.API_URL}/name/${id.toLowerCase()}`);
      const countryInfo = await api.json();
      setCountry(countryInfo);
    } catch (error) {
      console.log(error);
      
    }
    
  }
  const [getCountry, setCountry] = useState([]);
  useEffect(() => {
    getCountryInfo();
  }, [])
  
  const mapUrl = `https://maps.googleapis.com/maps/api/js?v3.exp&key=AIzaSyCU1sg0H8sWuzA4IvM1NsO-erYuvsrELTM`
  
  try {
    return (
      <div className="content-country">
        <BackButtonComponent/>
        {
        getCountry.map((country) => {
            const { name, flag, capital, region, languages, currencies, callingCodes, borders, latlng}:InCountry = country
           link.setAttribute('href', flag);
            return (
              <div key={name} className="card-container">
                <div className="content-flag">
                  <img src={flag} alt={name} title={name} />
                  <div className="map">
                    <Map googleMapURL={mapUrl}
                      containerElement={<div style={{ height: '200px' }}></div>}
                      mapElement={<div style={{ height: '100%' }}></div>}
                      loadingElement={<div>cargando</div>}
                      lat={Number(latlng[0])}
                      lon={Number(latlng[1])}
                    ></Map>
                  </div>
                </div>
                <div className="content-text">
                  <h1>{name}</h1>
                  <ul>
                    <li>Capital :  <span>{capital}</span></li>
                    <li>Region :  <span>{region}</span></li>
                    <li>Language :  {
                        <span> {languages.map(({ name }) =>  `${name} `)} </span>
                      }</li>
                    <li>Currencie :   {
                        <span> { currencies.map(({code})=> code )} </span>
                      
                      }</li>
                    <li>CallingCode :  {
                        <span> { callingCodes.map( c => c) } </span>
                      
                      }</li>
                  </ul>
                  <ul className="borders">
                    <h4>Borders <i className="fas fa-long-arrow-alt-right"></i></h4>
                    {
                     borders.length !==0 ?  borders.map(borde => {
                        
                        
                        return (
                          <li key={borde}>{ borde || 'Any border' }</li>
                        );
                     }) 
                        :
                      <li className="bordernon">Any border</li>
                    }
                  </ul>
                </div>
               
               
              </div>
              
            );
         }) 
        }
     </div>
    )
  } catch (error) {
    return (
      <div className="content-country">
        <BackButtonComponent/>
        <ErrorComponent/>
      </div>
      
    );
  }
  
  
}

interface InBackButton{}
const BackButtonComponent: React.FC<InBackButton> = () => {
  const backHome = () => {
    window.location.href = '/countries';
  }
  return (
    <>
    <div className="button-container">
      <button onClick={backHome} className="button-back">
          <i className="fas fa-long-arrow-alt-left"></i> Back
      </button>
    </div>
    </>  

  );
}

export default Flag;