import React from 'react';
import { GoogleMap, withScriptjs, withGoogleMap, Marker} from 'react-google-maps';
interface InMap{
  lat:number,
  lon: number
}
const MapComponent: React.FC<InMap> = (props) => {
  const {lat, lon } = props
  return (
    <GoogleMap
      defaultZoom={5}
      
      defaultCenter={{lat:lat, lng:lon}}
    >
      {
         <Marker position={{lat:lat, lng:lon}}/>
      }
    </GoogleMap>
  );
}


export default withScriptjs(
  withGoogleMap(
    MapComponent
  )
);

